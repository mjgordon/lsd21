import * as THREE from 'https://cdn.skypack.dev/three';

import * as keyboard from "./control/keyboard.js";
import * as mouse from "./control/mouse.js";
import { FloorPoly } from "./geometry/FloorPoly.js";
import { FloorOriented } from "./geometry/FloorOriented.js";
import { Cloud } from "./geometry/Cloud.js";
import { Orb } from "./Orb.js";
import { Player } from "./Player.js";


let camera, scene, renderer;
let player;

export let sound;

export let worldGeometry = [];
export let worldEntities = [];

let playerSphere;

let skyColor = 0xffffff;


function main() {
	scene = new THREE.Scene();

	camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
	camera.up.set(0, 0, 1);

	renderer = new THREE.WebGLRenderer();
	//renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setSize(1024, 768);
	renderer.shadowMap.enabled = true;
	//renderer.shadowMap.type = THREE.PCFSoftShadowMap;
	renderer.shadowMap.type = THREE.PCFShadowMap;
	renderer.setClearColor(skyColor);
	document.body.appendChild(renderer.domElement);

	mouse.setupPointerLock(renderer);

	player = new Player(new THREE.Vector3(2, 0, 0), Math.PI);

	new FloorOriented(scene,
		new THREE.Vector3(0, 0, 0),
		new THREE.Vector3(50, 50, 1));

	new FloorPoly(scene, new THREE.Vector3(1, 0.5, 1.2));

	const geometry = new THREE.SphereGeometry(0.1, 32, 16);
	const material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
	playerSphere = new THREE.Mesh(geometry, material);
	scene.add(playerSphere);

	const fog = new THREE.Fog(skyColor, 5, 20);
	scene.fog = fog;

	let innerFloor = new FloorPoly(scene,new THREE.Vector3(0,0,0),10);
	new Cloud(scene, innerFloor,10,0x885588);
	
	let orb = new Orb(scene, new THREE.Vector3(0, 0, 2));
	let orbCloud = new Cloud(scene,orb,0.10,0x0FFFF0,0.5,1);
	orbCloud.checkHeight = 2;
	orbCloud.speed = 0.02;

	setupLighting(scene);
	setupSound();

	animate();
}

function setupLighting(scene) {
	const light = new THREE.PointLight(0xFFFFFF, 1, 100);
	light.position.set(0, 0, 3);
	//scene.add(light);

	const skyColor = 0xB1E1FF;  // light blue
	const groundColor = 0xB97A20;  // brownish orange
	const light2 = new THREE.HemisphereLight(skyColor, groundColor, 0.3);
	light2.position.set(0, 0, 1);
	//scene.add(light2);

	const ambient = new THREE.AmbientLight(0xffffff, 0.5);
	scene.add(ambient);

	const sun = new THREE.DirectionalLight(0xffffff, 0.5);

	sun.castShadow = true;
	sun.shadow.mapSize.width = 512; // default
	sun.shadow.mapSize.height = 512; // default
	sun.shadow.camera.near = 0.1; // default
	sun.shadow.camera.far = 100;
	sun.position.set(0, 0, 1);
	scene.add(sun);
}


function setupSound() {
	const listener = new THREE.AudioListener();
	camera.add(listener);

	sound = new THREE.Audio(listener);

	const audioLoader = new THREE.AudioLoader();
	audioLoader.load('data/audio/pickup.wav', function(buffer) {
		sound.setBuffer(buffer);
	});
}


/**
Main loop. Delegates out to world updates and graphics rendering 
 */
function animate() {
	if (mouse.lockState == true) {
		update();
	}

	render();
}


/**
Executes a world tick
 */
function update() {
	/*
	worldGeometry.forEach(function(wg) {
		wg.update();
	});
	*/
	
	worldEntities.forEach(function(e) {
		e.update();
	});
	
	movePlayer();

	worldEntities.forEach(function(e) {
		e.collide(player);
	});

	let newEntities = [];
	worldEntities.forEach(function(e) {
		if (e.alive) {
			newEntities.push(e);
		}
		else {
			e.die(scene);
		}
	});
	worldEntities = newEntities;
}


/**
Draws the game
 */
function render() {
	updateCamera(player);
	requestAnimationFrame(animate);
	renderer.render(scene, camera);
}


/**
Sets the camera to the first-person view of entity
 */
function updateCamera(entity) {
	camera.position.set(entity.position.x, entity.position.y, entity.position.z + 2);

	var lookX = (Math.sin(entity.inclination) * Math.cos(entity.orientation)) + entity.position.x;
	var lookY = (Math.sin(entity.inclination) * Math.sin(entity.orientation)) + entity.position.y;
	var lookZ = Math.cos(entity.inclination) + entity.position.z + 2;

	camera.lookAt(lookX, lookY, lookZ);
}


/**
Reads keyboard state and moves player, reads mouse state and adjusts view
 */
function movePlayer() {
	var speed = 0.1;

	if ((keyboard.kState & keyboard.CONTROL_FORWARD) == keyboard.CONTROL_FORWARD) {
		player.velocity.x += Math.cos(player.orientation) * speed;
		player.velocity.y += Math.sin(player.orientation) * speed;
	}
	if ((keyboard.kState & keyboard.CONTROL_BACK) == keyboard.CONTROL_BACK) {
		player.velocity.x -= Math.cos(player.orientation) * speed;
		player.velocity.y -= Math.sin(player.orientation) * speed;
	}
	if ((keyboard.kState & keyboard.CONTROL_RIGHT) == keyboard.CONTROL_RIGHT) {
		player.velocity.x += Math.cos(player.orientation - (Math.PI / 2)) * speed;
		player.velocity.y += Math.sin(player.orientation - (Math.PI / 2)) * speed;
	}
	if ((keyboard.kState & keyboard.CONTROL_LEFT) == keyboard.CONTROL_LEFT) {
		player.velocity.x += Math.cos(player.orientation + (Math.PI / 2)) * speed;
		player.velocity.y += Math.sin(player.orientation + (Math.PI / 2)) * speed;
	}
	if ((keyboard.kState & keyboard.CONTROL_SPACE) == keyboard.CONTROL_SPACE) {
		if (player.jumpState == false) {
			player.velocity.z += 0.25;
			player.jumpState = true;
		}

	}

	//player.position.add(player.velocity);
	player.position = player.getUpdatedPosition();
	worldGeometry.forEach(function(wg) {
		wg.collide(player);
	});

	playerSphere.position.copy(player.position);

	player.orientation -= mouse.mouseDX / 600.0;
	player.inclination += mouse.mouseDY / 600.0;
	player.inclination = Math.min(Math.max(player.inclination, 0.001), Math.PI - 0.001);

	mouse.resetMouseDelta();
}

main();

import * as THREE from 'https://cdn.skypack.dev/three';

import { worldEntities } from "./lsd21.js";

const gravity = -0.010;

export class Entity {
	constructor(position, orientation = 0) {
		this.position = position;
		this.orientation = orientation;
		this.velocity = new THREE.Vector3(0, 0, 0);
		this.inclination = Math.PI / 2;
		this.maxSpeed = 0.2;

		this.jumpState = false;

		// Automatically checked in final update loop to remove entity if necessary
		this.alive = true;

		worldEntities.push(this);
	}

	getUpdatedPosition() {
		let positionNew = this.position.clone();

		let tempZ = this.velocity.z;
		tempZ += gravity;
		tempZ = Math.min(this.maxSpeed, Math.max(tempZ, this.maxSpeed * -2));
		this.velocity.clampLength(0, this.maxSpeed);

		positionNew.add(this.velocity);

		this.velocity.multiplyScalar(0.5);
		this.velocity.z = tempZ;

		return (positionNew);
	}
	
	update() { }

	collide(_entity) { }

	die(scene) { 
		scene.remove(this.mesh);
	}

	setPosition(_position) {
		this.position.copy(_position);
		this.mesh.position.copy(_position);
	}

}
export let kState = 0;

export const CONTROL_FORWARD = 1;
export const CONTROL_RIGHT   = 2;
export const CONTROL_BACK    = 4;
export const CONTROL_LEFT    = 8;
export const CONTROL_SPACE   = 16;

document.onkeydown = function(e) {
	switch(e.keyCode) {
		case 87:
			kState |= CONTROL_FORWARD;
		break;
			
		case 68:
			kState |= CONTROL_RIGHT;
		break;
		
		case 83:
			kState |= CONTROL_BACK;
		break;
		
		case 65:
			kState |= CONTROL_LEFT;
		break;
		
		case 32:
			kState |= CONTROL_SPACE;
		break; 
	}
	
}

document.onkeyup = function(e) {
	switch(e.keyCode) {
		case 87:
			kState ^= CONTROL_FORWARD;
		break;
			
		case 68:
			kState ^= CONTROL_RIGHT;
		break;
		
		case 83:
			kState ^= CONTROL_BACK;
		break;
		
		case 65:
			kState ^= CONTROL_LEFT;
		break;
		
		case 32:
			kState ^= CONTROL_SPACE;
		break;
	}
	
}

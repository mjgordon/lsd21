export let mouseX = 0;
export let mouseY = 0;
export let pmouseX = 0;
export let pmouseY = 0;
export let mouseDX = 0;
export let mouseDY = 0;
export let lockState = false;





export function setupPointerLock(renderer) {
	renderer.domElement.requestPointerLock = renderer.domElement.requestPointerLock ||
		renderer.domElement.mozRequestPointerLock;
	renderer.domElement.addEventListener('click', function() {
		renderer.domElement.requestPointerLock();
	});

	document.addEventListener('pointerlockchange', lockChangeAlert, false);
	document.addEventListener('mozpointerlockchange', lockChangeAlert, false);

	function lockChangeAlert() {
		if (document.pointerLockElement === renderer.domElement ||
			document.mozPointerLockElement === renderer.domElement) {
			console.log('The pointer lock status is now locked');
			//document.addEventListener("mousemove", updatePosition, false);
			lockState = true;
		} else {
			console.log('The pointer lock status is now unlocked');
			//document.removeEventListener("mousemove", updatePosition, false);
			lockState = false;
		}
	}
}

document.onmousemove = function(e) {
	pmouseX = mouseX;
	pmouseY = mouseY;

	mouseX = e.clientX;
	mouseY = e.clientY;

	mouseDX = e.movementX;
	mouseDY = e.movementY;

	//	console.log(e);
}

export function resetMouseDelta() {
	mouseDX = 0;
	mouseDY = 0;
}
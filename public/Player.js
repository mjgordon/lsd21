import {Entity} from "./Entity.js";

export class Player extends Entity {
	constructor(position,orientation) {
		super(position,orientation);
		this.orbCount = 0;
	}
	
	getOrb() {
		this.orbCount += 1;
		console.log(this.orbCount);
	}
}
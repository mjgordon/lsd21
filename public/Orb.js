import * as THREE from 'https://cdn.skypack.dev/three';
import { Entity } from "./entity.js";
import { Player } from "./Player.js";
import { sound } from "./lsd21.js";

export class Orb extends Entity {
	constructor(scene,position) {
		super(position);
		this.radius = 0.15;
		let geometry = new THREE.SphereGeometry(this.radius,32,16);
		let material = new THREE.MeshBasicMaterial({color: 0xffff00});
		this.mesh = new THREE.Mesh(geometry,material);
		this.mesh.position.copy(position);
		
		this.light = new THREE.PointLight(0xffff44,1,5,2);
		this.light.position.copy(position);
		this.light.castShadow = true;
		
		this.cloudParent = null;
		
		scene.add(this.mesh);
		scene.add(this.light);
	}
	
	collide(entity) {
		let ed = entity.position.clone();
		if (Math.abs(ed.z - this.position.z) <=2) {
			ed.z = this.position.z;
		}
		
		const d = this.position.distanceTo(ed);
		
		if (d < this.radius * 2) {
			this.alive = false;
			if (this.cloudParent) {
				this.cloudParent.alive = false;
			}
			if (entity instanceof Player) {
				entity.getOrb();
			}
			sound.play();
		}
		
	}
	
	die(scene) {
		super.die(scene);
		scene.remove(this.light);
	}
	
	setPosition(position) {
		super.setPosition(position);
		this.light.position.copy(position);
	}
}
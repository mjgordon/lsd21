import * as THREE from 'https://cdn.skypack.dev/three';

import { Entity } from "./../Entity.js"
import { Geometry } from "./Geometry.js";
import { FloorPoly } from "./FloorPoly.js";

export class Cloud extends Entity {


	constructor(scene, innerObject, radius, color, levelThickness = 2,maxDelta = 3) {
		super();

		this.levels = 40;
		this.levelThickness = levelThickness
		this.vertexCount = 32;
		this.speed = 0.01;

		this.vertices = [];
		
		this.checkHeight = 1;
		
		this.maxDelta = maxDelta;

		for (var i = 0; i < this.vertexCount; i++) {
			let n = 1.0 * i / this.vertexCount * Math.PI * 2;
			this.vertices.push(new THREE.Vector3(Math.cos(n) * radius, Math.sin(n) * radius, 0));
		}

		this.positions = new Float32Array(this.levels * this.vertexCount * 3)
		this.normals = new Float32Array(this.levels * this.vertexCount * 3)
		this.offsetFrames = [];

		let indices = [];

		for (var i = 0; i < this.levels - 1; i++) {
			for (var j = 0; j < this.vertexCount; j++) {
				const a = (i * this.vertexCount) + j;
				const b = (i * this.vertexCount) + ((j + 1) % this.vertexCount);
				const c = ((i + 1) * this.vertexCount) + j;
				const d = ((i + 1) * this.vertexCount) + ((j + 1) % this.vertexCount);
				indices.push(a, b, d);
				indices.push(a, d, c);
			}
		}

		this.geometry = new THREE.BufferGeometry();
		this.geometry.setIndex(indices);
		this.geometry.setAttribute('position', new THREE.Float32BufferAttribute(this.positions, 3));
		this.geometry.setAttribute('normal', new THREE.Float32BufferAttribute(this.normals, 3));

		this.generatePositions();

		let material = new THREE.MeshPhongMaterial({
			color: color,
			opacity: 0.5,
			transparent: true,
			side: THREE.DoubleSide
		});

		this.mesh = new THREE.Mesh(this.geometry, material);

		this.offset = 0;

		this.innerObject = innerObject;
		innerObject.cloudParent = this;

		scene.add(this.mesh);
	}


	update() {
		let d = this.maxDelta;
		this.offset = (this.offset += this.speed) % this.levelThickness;
		if (this.offset < this.speed) {
			this.offsetFrames.shift();
			
			let last = new THREE.Vector3();
			if (this.offsetFrames.length > 1) {
				last.copy(this.offsetFrames[this.offsetFrames.length - 1]);
			}

			let dx = Math.random() * d - (d / 2) + last.x;
			let dy = Math.random() * d - (d / 2) + last.y;
			//let dz = i * 2;
			this.offsetFrames.push(new THREE.Vector3(dx, dy, 0));
			this.generatePositions();

			this.mesh.geometry.attributes.position.needsUpdate = true;
		}

		this.mesh.position.z = -this.offset;

		let start = Math.floor((this.checkHeight + this.offset) / this.levelThickness);
		let plus = ((this.checkHeight + this.offset) / this.levelThickness) % 1;
		let innerPos = this.offsetFrames[start].clone();
		innerPos.lerp(this.offsetFrames[start + 1], plus);
		innerPos.z = this.checkHeight;
		this.innerObject.setPosition(innerPos);
	}

	generatePositions() {
		let d = this.maxDelta;
		for (var i = 0; i < this.levels; i++) {
			let last = new THREE.Vector3();
			if (this.offsetFrames.length > 1) {
				last.copy(this.offsetFrames[this.offsetFrames.length - 1]);
			}
			let dx = Math.random() * d - (d / 2) + last.x;
			let dy = Math.random() * d - (d / 2) + last.y;
			//let dz = i * 2;
			this.offsetFrames.push(new THREE.Vector3(dx, dy, 0));

			for (var j = 0; j < 32; j++) {
				let id = (i * 32 + j) * 3;
				let pos = this.vertices[j].clone();
				pos.add(this.offsetFrames[i]);
				pos.z += i * this.levelThickness;
				this.geometry.attributes.position.array[id] = pos.x;
				this.geometry.attributes.position.array[id + 1] = pos.y;
				this.geometry.attributes.position.array[id + 2] = pos.z;
				//this.positions[id] = pos.x;
				//this.positions[id + 1] = pos.y;
				//this.positions[id + 2] = pos.z;
				this.geometry.attributes.normal.array[id] = this.vertices[j].x;
				this.geometry.attributes.normal.array[id + 1] = this.vertices[j].y;
				this.geometry.attributes.normal.array[id + 2] = this.vertices[j].z;
				//this.normals.push(this.vertices[j].x, this.vertices[j].y, this.vertices[j].z);
			}
		}
	}
}
import { worldGeometry } from "./../lsd21.js";

export class Geometry {
	constructor() {
		this.mesh = null;
	}
	
	collide(_entity) {}
	
	update() {}
	
	addToWorld() {
		worldGeometry.push(this);
	}
	
	setPosition(_position) {}
}
import * as THREE from 'https://cdn.skypack.dev/three';

import { Geometry } from "./Geometry.js";

export class FloorOriented extends Geometry {
	constructor(scene,position, dimensions) {
		super();
		this.position = position;
		this.dimensions = dimensions;
		this.halfDimensions = dimensions.clone().divideScalar(2);
		
		const geometry = new THREE.BoxGeometry(dimensions.x, dimensions.y, dimensions.z);
		const material = new THREE.MeshPhongMaterial({ color: 0x558855 });
		this.mesh = new THREE.Mesh(geometry, material);
		this.mesh.position.set(position.x,position.y,position.z - this.halfDimensions.z);
		this.mesh.castShadow = true;
		this.mesh.receiveShadow = true;
		
		scene.add(this.mesh);
		this.addToWorld();
	}

	collide(entity) {
		if (entity.position.x >= this.position.x - this.halfDimensions.x &&
			entity.position.y >= this.position.y - this.halfDimensions.y &&
			entity.position.x <= this.position.x + this.halfDimensions.x &&
			entity.position.y <= this.position.y + this.halfDimensions.y) {
			if (entity.position.z < this.position.z &&
				entity.position.z > this.position.z - this.dimensions.z) {
				entity.position.z = this.position.z;
				entity.velocity.z = 0;
				entity.jumpState = false;
			}
		}
	}
}


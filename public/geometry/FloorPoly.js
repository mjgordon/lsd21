import * as THREE from 'https://cdn.skypack.dev/three';
import { Geometry } from "./geometry.js";

export class FloorPoly extends Geometry {

	constructor(scene, position, radius = 2) {
		super();

		this.position = position;
		//this.vertices = vertices;
		this.vertices = [];

		let shape = new THREE.Shape();
		for (var i = 0; i <= 24; i++) {
			var n = i / 24.0 * Math.PI * 2;
			let x = Math.cos(n) * radius;
			let y = Math.sin(n) * radius;
			shape.lineTo(x, y);
			this.vertices.push(new THREE.Vector3(x,y,position.z));
		}

		const extrudeSettings = {
			steps: 2,
			depth: 0.3,
			bevelEnabled: true,
			bevelThickness: 0.03,
			bevelSize: 0.01,
			bevelOffset: 0,
			bevelSegments: 1
		}

		//let geometry = new THREE.ShapeGeometry(shape);
		let geometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
		let material = new THREE.MeshPhongMaterial({
			color: 0x885588,
			opacity: 0.5,
			transparent: false
		});
		this.mesh = new THREE.Mesh(geometry, material);
		this.mesh.position.copy(position);
		this.mesh.rotation.x = Math.PI;
		this.mesh.receiveShadow = true;
		this.mesh.castShadow = true;
		
		scene.add(this.mesh);
		this.addToWorld();
	}

	collide(entity) {

		if (this.checkInclusion(entity.position)) {
			let thickness = entity.velocity.z > 0 ? 0.05 : 0.5;
			if (entity.position.z < this.position.z &&
				entity.position.z > this.position.z - thickness) {
				entity.position.z = this.position.z;
				entity.velocity.z = 0;
				entity.jumpState = false;
			}
		}
	}

	checkInclusion(point) {
		let vs = this.vertices;
		var x = point.x - this.position.x,
			y = point.y - this.position.y;

		var inside = false;
		for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
			var xi = vs[i].x,
				yi = vs[i].y;
			var xj = vs[j].x,
				yj = vs[j].y;

			var intersect = ((yi > y) != (yj > y)) &&
				(x < (xj - xi) * (y - yi) / (yj - yi) + xi);
			if (intersect) inside = !inside;
		}

		return inside;

	}
	
	setPosition(position) {
		this.position.copy(position);
		this.mesh.position.copy(position);
	}

}